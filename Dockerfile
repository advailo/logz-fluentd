FROM fluent/fluentd:v1.2.5
LABEL maintainer Rolands <rolands@advailo.com>
WORKDIR /home/fluent
ENV PATH /home/fluent/.gem/ruby/2.2.0/bin:$PATH
RUN gem install fluent-plugin-secure-forward \
&& gem install fluent-plugin-logzio
EXPOSE 24284
ENTRYPOINT []
CMD fluentd -c /fluentd/etc/$FLUENTD_CONF -p /fluentd/plugins $FLUENTD_OPT